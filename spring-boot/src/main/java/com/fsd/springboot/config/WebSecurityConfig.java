package com.fsd.springboot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fsd.springboot.core.security.MyUserDetailService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	 @Autowired
	    public void configureGlobal(AuthenticationManagerBuilder  auth) throws  Exception{
	        auth.userDetailsService(customUserDetailsService())
	                .passwordEncoder(passwordEncoder());

	 }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.csrf().disable().authorizeRequests()
		
		.antMatchers("/h2-console/**").permitAll()

		.antMatchers("/**").hasRole("USER")

		.anyRequest().permitAll()
		
		.and()
		
		.formLogin().defaultSuccessUrl("/login/user").permitAll(); // 设置登陆成功页
		
		// 解决异常： Refused to display 'url' in a frame because it set 'X-Frame-Options' to 'deny'.
		http.headers().frameOptions().sameOrigin();
		
	}
 
	/*
	 * @Override public void configure(WebSecurity web) throws Exception { //
	 * 设置拦截忽略文件夹，可以对静态资源放行 web.ignoring().antMatchers("/css/**", "/js/**");
	 * 
	 * }
	 */
	
	/**
     * 设置用户密码的加密方式
     * @return
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
 
    }
 
    /**
     * 自定义UserDetailsService，授权
     * @return
     */
    @Bean
    public MyUserDetailService customUserDetailsService(){
        return new MyUserDetailService();
    }
 
    /**
     * AuthenticationManager
     * @return
     * @throws Exception
     */
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();

    }	
	
}
