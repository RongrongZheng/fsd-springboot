package com.fsd.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fsd.springboot.core.base.ResponseResult;
import com.fsd.springboot.entity.LabUser;
import com.fsd.springboot.service.UserService;

@RestController
@RequestMapping({"/login"})

class LoginController {
	
	@Autowired
	private UserService userService;
	
	
	@RequestMapping("/user") //mapping映射url路径
	public ResponseResult index() {
		List<LabUser> users = null;
    	
    	if (users==null || users.isEmpty()) 
    		return new ResponseResult("0","sucessful",null);
    	else 
    		return new ResponseResult("1","failed",users);
    }
	
	
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseResult<LabUser> checkUser(@RequestBody LabUser user) {
    	
    	List<LabUser> users = userService.findUser(user);
    	
    	if (users==null || users.isEmpty()) 
    		return new ResponseResult<LabUser>("0","failed",users);
    	else 
    		return new ResponseResult<LabUser>("1","sucessful",null);
    	
        
    }
}