package com.fsd.springboot.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain=true)
@Entity
@Table(name="lab_user")
public class LabUser {
	
	@Id
	private long id;
	private String username;
	private String email;
	private String password;
	private String fullname;
	
}
