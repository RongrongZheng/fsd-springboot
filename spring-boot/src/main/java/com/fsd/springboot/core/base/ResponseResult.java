package com.fsd.springboot.core.base;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain=true)
@NoArgsConstructor                 //无参构造
@AllArgsConstructor 
public class ResponseResult<T> {
	
	private String status;
	private String message;
	private List<T> list;
	
	
	
}
