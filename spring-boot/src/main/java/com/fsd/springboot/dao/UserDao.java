package com.fsd.springboot.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.fsd.springboot.entity.LabUser;

@Repository
public class UserDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	public List<LabUser> findByName(String username) {
		
		String sql = "select * from lab_user where name='"+username+"' ";
		// 创建一个新的BeanPropertyRowMapper对象
		RowMapper<LabUser> rowMapper = new BeanPropertyRowMapper<LabUser>(LabUser.class);
		List<LabUser> users=this.jdbcTemplate.query(sql, rowMapper, null);
		// 将id绑定到SQL语句中，通过RowMapper返回一个Object类型的单行记录
		return users;
	}
	
	public List<LabUser> findById(String id) {
		
		String sql = "select * from lab_user where id='"+id+"' ";
		RowMapper<LabUser> rowMapper = new BeanPropertyRowMapper<LabUser>(LabUser.class);
		return this.jdbcTemplate.query(sql, rowMapper, null);
	}


	public void update(LabUser user) {
		String sql = "update lab_user set NAME='"+user.getUsername()+"', "
				+ "PASSWORD='"+user.getPassword()+"',"
				+ "EMAIL='"+user.getEmail()+"' where id='"+user.getId()+"'";
		
		//List<Object> params = this.getParamsList(user);
		this.jdbcTemplate.update(sql);
		
	}
	
	public void insert(LabUser user) {
		String sql = "INSERT INTO LAB_USER (NAME,PASSWORD,EMAIL,ID) "
				+ "VALUES('"+user.getUsername()+"',"
						+ "'"+user.getPassword()+"',"
						+ "'"+user.getEmail()+"',"
						+ "'"+user.getId()+"')";
		//List<Object> params = this.getParamsList(user);
		this.jdbcTemplate.update(sql);
	}


	private List<Object> getParamsList(LabUser user) {
		List<Object> params= new ArrayList<>();
		
		params.add(user.getUsername());
		params.add(user.getPassword());
		params.add(user.getEmail());
		params.add(user.getId());
		
		return params;
	}


	

}
