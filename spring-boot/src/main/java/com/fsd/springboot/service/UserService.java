package com.fsd.springboot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import com.fsd.springboot.core.base.ResponseResult;
import com.fsd.springboot.dao.UserDao;
import com.fsd.springboot.entity.LabUser;

@Service
public class UserService {
	
	@Autowired
	private UserDao userDao;
	

	public ResponseResult<LabUser> update(LabUser user) {
		
		List<LabUser> users=userDao.findById(String.valueOf(user.getId()));
		if(users==null || users.isEmpty() )
			userDao.insert(user);
		else
			userDao.update(user);
		List<LabUser> labusers= new ArrayList<LabUser>();
		users.add(user);
    	return new ResponseResult<LabUser>("1","sucessful",labusers);
		
	}
	
	public List<LabUser> findUser(LabUser user) {
		
		List<LabUser> labusers=userDao.findByName(user.getUsername());
		
		if(labusers == null || labusers.isEmpty()) return null;
		
		if(user.getPassword().equals(labusers.get(0).getPassword())) return labusers;
		
		return null;
		
	}

	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<String> findPermissions(String username) {
		// TODO Auto-generated method stub
		return null;
	}

}
